$(document).ready(function () {
    $('#popupNewProject').on('click', function () {
        $('.overlayPopup').css('display', 'none')
    })

    $('.optionDots').on('click', function () {
        $(this).find('.optionWrapper').toggleClass('showDiv')
    })

    $("#range").slider({
        range: "min",
        max: 100,
        value: 50,
        slide: function(e, ui) {
            $("#currentVal").html(ui.value);
        }
    });

})

var activeOptions = true;

function showOptions(index) {
    if (activeOptions) {
        $('#'+index).css('display', 'block')
        activeOptions = false;
    } else {
        $('#'+index).css('display', 'none')
        activeOptions = true;
    }
}
function addNewProject() {
    $('.overlayPopup').css('display', 'flex')
}

var activeChatSidebar = true;
function expandChat() {
    if (activeChatSidebar) {
        $('.chatSection').animate({
            "right":"-300px"
        }, 500)
        activeChatSidebar = false;
    } else {
        $('.chatSection').animate({
            "right":"0"
        }, 500)
        activeChatSidebar = true;
    }
}


(function() {
    var activechatBox = true;
    $('#live-chat header').on('click', function() {
        $('.chat').slideToggle(300, 'swing');
        $('.counter').fadeToggle(300, 'swing');

        if (activechatBox) {
            $('.counter').addClass('flex');
            activechatBox = false;
        } else {
            $('.counter').removeClass('flex');
            activechatBox = true;
        }

    });
    $('.chatClose').on('click', function(e) {
        e.preventDefault();
        $('#live-chat').fadeOut(300);
    });

}) ();
